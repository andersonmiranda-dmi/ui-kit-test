import React from 'react';

import { BackButton } from './back-button';

export default {
  component: BackButton,
  title: 'Components/BackButton',
}
export const Default = () => <BackButton>Textlink</BackButton>;
export const Hover = () => <BackButton>Textlink</BackButton>;
Hover.parameters = { pseudo: { hover: true } }

