import React from 'react';

import { TextButton } from './text-button';

export default {
  component: TextButton,
  title: 'Components/TextButton',
}
export const Default = () => <TextButton>Text button</TextButton>;
export const Hover = () => <TextButton>Text button</TextButton>;
Hover.parameters = { pseudo: { hover: true } }