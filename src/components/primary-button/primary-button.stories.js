import React from 'react';

import { PrimaryButton } from './primary-button';

export default {
  component: PrimaryButton,
  title: 'Components/PrimaryButton',
}
export const Default = () => <PrimaryButton>primary button</PrimaryButton>;
export const Disabled = () => <PrimaryButton disabled>primary button</PrimaryButton>;
export const Hover = () => <PrimaryButton>primary button</PrimaryButton>;
Hover.parameters = { pseudo: { hover: true } }
export const Focus = () => <PrimaryButton>primary button</PrimaryButton>;
Focus.parameters = { pseudo: { focus: true } }

