import { Button as OriginButton } from "semantic-ui-react";

export const PrimaryButton = (props) => <OriginButton primary {...props} />;
