import { Button as OriginButton } from "semantic-ui-react";

export const CloseButton = (props) => <OriginButton className="close-icon" {...props} />;
