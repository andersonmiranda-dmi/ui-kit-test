import React from 'react';

import { CloseButton } from './close-button';

export default {
  component: CloseButton,
  title: 'Components/CloseButton',
}
export const Default = () => <CloseButton />;
export const Hover = () => <CloseButton />;
Hover.parameters = { pseudo: { hover: true } }