import React from 'react';

import { SecondaryButton } from './secondary-button';

export default {
  component: SecondaryButton,
  title: 'Components/SecondaryButton',
}
export const Default = () => <SecondaryButton>secondary button</SecondaryButton>;
export const Disabled = () => <SecondaryButton disabled>secondary button</SecondaryButton>;
export const Hover = () => <SecondaryButton>secondary button</SecondaryButton>;
Hover.parameters = { pseudo: { hover: true } }
export const Focus = () => <SecondaryButton>secondary button</SecondaryButton>;
Focus.parameters = { pseudo: { focus: true } }
