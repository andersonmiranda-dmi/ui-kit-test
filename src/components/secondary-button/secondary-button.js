import { Button as OriginButton } from "semantic-ui-react";

export const SecondaryButton = (props) => <OriginButton secondary {...props} />;
